+++
title = 'Enhancing SSH Security with <span class="base2A">fail2ban</span> and Two-Factor Authentication'
date = 2024-06-20T10:43:04+03:00
description = "Secure your SSH server with advanced techniques including cryptographic keys, Google Authenticator for 2FA, fail2ban for host IP banning. Follow this guide for a step-by-step setup to enhance security"
draft = false
+++

Have you ever wondered how to secure your SSH server at a higher level? Today, I'm going to show you how to configure your SSH server to restrict password authentication (using cryptographic keys), enable two-factor authentication (2FA) with Google Authenticator, and configure <span class="base0A">fail2ban</span> to block hosts that repeatedly fail to authenticate. This comprehensive setup significantly enhances your server's security.
1. Setting up <span class="base0A">fail2ban</span>

<span class="base0A">fail2ban</span> is an open-source daemon that scans log files like <span class="base1D">/var/log/auth.log</span> for failed authentication attempts and bans IP addresses that fail multiple times, enhancing your server's security. Keep in mind that low password security or not using a password manager is still a security flaw that needs addressing.

To install <span class="base0A">fail2ban</span> on a Debian-based Linux distribution, run the following command:

```
sudo apt-get install fail2ban -y
```

Once installed, the configuration file is located at <span class="base0D">/etc/fail2ban/jail.conf</span>. The configuration is well-documented and easy to understand. Let's make a backup of the configuration file:
<br>

```
sudo cp fail2ban.conf fail2ban.conf.backup
```

In the configuration file, add the following lines to specify that <span class="base0A">fail2ban</span> should monitor SSH login attempts. It will also track 2FA attempts and ban IP addresses if more than three OTP codes are incorrect.

```
# [sshd]
enabled = true
bantime = 4w
maxretry = 3
```

After this, <span class="base0A">fail2ban</span> will be set up to monitor authentication attempts and create firewall rules as needed.

# Setting up the Google Authenticator PAM module
<br>
Firstly, what is PAM?

PAM (Pluggable Authentication Modules) is a suite of libraries that enables administrators to set up various authentication methods for users. In our case, we want to leverage the security benefits of 2FA for SSH connections.

```
┌──(beowolf23㉿box)-[~]
└─$ sudo apt-get install libpam-google-authenticator -y

┌──(beowolf23㉿box)-[~]
└─$ google-authenticator

=== QR CODE ===

Enter code from app (-1 to skip): 014017
Code confirmed                                                                                                                                                     
Your emergency scratch codes are:                                                                                                                                  
  80408877                 
  52768797 
  44942598
  45896901
  47732904

```

You will be prompted with a QR code to be scanned from the Google Authenticator app on your mobile device. What happens here is that, by scanning the QR code, the Authenticator app will receive the secret key used for generating the time based OTPs. Also, some emergency codes will be generated and those are for the unfortunate cases when you will lose your phone :confused:.

**Modify SSHD PAM configuration to include the Google Authenticator module**
```
┌──(beowolf23㉿box)-[~]
└─$ sudo vim /etc/pam.d/sshd

# Disable Standard Linux Authentication; we want the custom module to be used instead.
# @include common-auth 

auth required pam_google_authenticator.so
auth required pam_permit.so
```

**Modify the <span class="base0D">/etc/ssh/sshd_config</span> file to allow the new authentication method:**

```
AuthenticationMethods publickey,keyboard-interactive
PasswordAuthentication no
```

**Restart the SSH Server**
```
┌──(beowolf23㉿box)-[~]
└─$ sudo service ssh restart
```

If you encounter issues while restarting the SSH server, you may need to remove the `KbdInteractiveAuthentication` key if set.

**Testing the setup**
```
──(beowolf23㉿box)-[~]
└─$ ssh beowolf23@box            
(beowolf23@box) Verification code: 
Last login: Thu Jun 20 14:53:07 2024 from 127.0.0.1
```

That's it! In just a few minutes, you've significantly improved your server's security by using <span class="base0A">fail2ban</span> for IP bans and integrating Google Authenticator with PAM for 2FA.