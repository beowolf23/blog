+++
title = 'Google Dorking'
date = 2024-06-18T16:32:29+03:00
description = "Short description of how to use Google Dorks for narrowing search results"
draft = false
+++

Google Dorking is a powerful searching technique that makes use of query operators to search pages that are not easily accessible by traditional search methods. 

Some of these operators are:
- site: This operator filters results based on the specified domain.
- filetype: This operator filters results based on the filetype. Basically, using this, you can get only PDF results for a specific search.
- inurl: This operator lets you search for websites that contain a specific string in the URL.
- intext: This operator lets you search for websites that contain a specific text in the page.

For example, if you want to read the new edition of The Ethical Hacker Playbook, your search would look like this:

**filetype: pdf the ethical hacker playbook**

{{< figure src="./images/search-results.png" title="Basic Google Dorking usage">}}

This is a basic example of how you can apply operators to your search to narrow down the results, and it is an ethical way to use it :smile:. Of course, this can be used in more unethical searches, as done by hackers, penetration testers who want to retrieve as much data as possible hoping for leaks in the pages returned. 

One useful tool for using advanced searching is the Google Hacking tool by pentest-tools.com. 

You can search based on their available Google Dorks as you can see in the image below. In this case, I searched for login pages for the example.com website.
{{< figure src="./images/pentesttools-search.png" title="Basic Google Dorking usage">}}

The full list of available Google Dorks that you can use from the Google Hacking tool is down below:
{{< figure src="./images/pentesttools-availableoptions.png" title="Basic Google Dorking usage">}}