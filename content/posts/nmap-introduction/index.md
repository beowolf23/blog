+++
title = 'Quick Nmap Intro'
date = 2024-06-21T11:42:46+03:00
description = 'Learn the basics of using Nmap> for network discovery and port scanning, including practical examples and Wireshark analysis. Discover how to perform ethical and legal scans to enhance your network security skills. 🚀🔍🔒'
draft = false
+++

<span class="base0A">Nmap</span> or Network Mapper is a security tool used for discovery purposes through it's port scanning and network exploration features. Besides the basic scanning, it offers support for writing custom scripts in addition to the existing vulnerability scanning ones.

In this post, we will take a look on how to use <span class="base0A">Nmap</span> and understand how port scanning works. Keep in mind that port scanning is considered illegal unless authorized by the 'victim'. For this guide's scope, I will be using a Kali Linux box as the attacker and a MacOS host as the victisudo nmap -sLm.

<span class="base0A">Nmap</span> is available for most operating systems and can be installed from the official website or by using Package Managers. In Kali Linux, which is a Debian-based Linux distro, it can be installed with:

```
sudo apt-get install nmap -y
```

You can try running `nmap -h` to get a view of the available options (which are actually a lot :flushed:). 

One of the simplest yet powerful commands in <span class="base0A">Nmap</span> is the sudo nmap -sn command. This command performs a "ping scan," which is used to discover hosts within a specified network without conducting a port scan. Essentially, it tells you which devices are up and running on your network.

For instance, if you want to scan a network with the IP range 192.168.1.0/24, you would run:
```
┌──(beowolf23㉿box)-[~]
└─$ nmap -sn 192.168.1.1/24
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-06-21 12:03 EEST
Nmap scan report for 192.168.1.1
Host is up (0.0046s latency).
Nmap scan report for 192.168.1.132
Host is up (0.089s latency).
Nmap scan report for 192.168.1.133
Host is up (0.048s latency).
Nmap scan report for 192.168.1.134
Host is up (0.00035s latency).
Nmap done: 256 IP addresses (4 hosts up) scanned in 2.62 seconds
```

This command will send ICMP echo requests (ping), TCP SYN packets, or ARP requests (depending on the network setup) to each address in the specified range and report which ones respond. This is a quick way to map out the active devices on your network, which is particularly useful for identifying new or unauthorized devices.

Moving forward, let's delve into a more detailed scan with the nmap -sT command. This is a full TCP connect scan, targeting the most common 1000 ports on the specified IP address. To execute this scan, you would use:

```
┌──(beowolf23㉿box)-[~]
└─$ sudo nmap -sT 192.168.1.133
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-06-21 12:12 EEST
Nmap scan report for 192.168.1.133
Host is up (0.0038s latency).
Not shown: 996 closed tcp ports (conn-refused)
PORT     STATE SERVICE
3306/tcp open  mysql
5000/tcp open  upnp
7000/tcp open  afs3-fileserver
8080/tcp open  http-proxy
MAC Address: 4C:20:B8:E9:6B:29 (Apple)
Nmap done: 1 IP address (1 host up) scanned in 5.47 seconds
```

This command will establish a complete TCP connection with each port to determine its status. Here's how it works:

1. SYN Sent: <span class="base0A">Nmap</span> sends a TCP SYN (synchronize) packet to the target port.
2. SYN-ACK Received: If the port is open, the target responds with a TCP SYN-ACK (synchronize-acknowledge) packet.
3. ACK Sent: <span class="base0A">Nmap</span> sends an ACK (acknowledge) packet back to the target, establishing a full connection.
4. RST Sent: To close the connection immediately, <span class="base0A">Nmap</span> sends a RST (reset) packet.

If a port is closed, the target will respond with an RST packet instead of a SYN-ACK. If no response is received, the port is considered filtered (likely by a firewall). This method, although slower than other scan types like SYN scan (-sS), is more reliable and can be run without special privileges, making it accessible for users with limited permissions.

Wireshark provides a detailed view of each packet, including the source and destination IP addresses, port numbers, and the flags set in the TCP headers. Let's take a look into the Wireshark report to better understand how the full TCP Scan works.

In this example, I did a full TCP Scan with no ports specified, capturing the packets with Wireshark. I applied the following filter to get the results for a single port.

```
(ip.addr eq 192.168.1.nd ip.addr eq 192.168.1.134) and (tcp.port eq 8080)
```

{{< figure src="./images/wireshark.png" title="Wireshark capture of full TCP Scan" >}}

So, as seen in the report above, the 3-way handshake is performed between the two machines. The downside of this is that modern firewalls and Intrusion Detection Systems easily detect this kind of traffic as it generates a lot of noise when doing a scan on hundreds of ports (like the one above). 

To be more stealthy, we can use the `-sS` option which performs a SYN scan or a Half-open scan. 

Here's how it works:

1. SYN Sent: <span class="base0A">Nmap</span> sends a TCP SYN (synchronize) packet to the target port.
2. SYN-ACK Received: If the port is open, the target responds with a TCP SYN-ACK (synchronize-acknowledge) packet.
3. RST Sent: To close the connection immediately, <span class="base0A">Nmap</span> sends a RST (reset) packet.

So, immediately after the SYN-ACK packet is received, we will send a RESET (RST) packet back, basically saying goodbye.

Here's how this would be done in practice.

```
┌──(beowolf23㉿box)-[~]
└─$ sudo nmap -sS 192.168.1.133
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-06-21 12:42 EEST
Nmap scan report for 192.168.1.133
Host is up (0.0058s latency).
Not shown: 996 closed tcp ports (reset)
PORT     STATE SERVICE
3306/tcp open  mysql
5000/tcp open  upnp
7000/tcp open  afs3-fileserver
8080/tcp open  http-proxy
MAC Address: 4C:20:B8:E9:6B:29 (Apple)

Nmap done: 1 IP address (1 host up) scanned in 4.14 seconds
```

Same result as with the full TCP 3-way handshake, but less noisy, demonstrated by another Wireshark scan.

{{< figure src="./images/wireshark2.png" title="Wireshark capture of SYN Scan" >}}

We can see that the conversation is terminated after receiving the SYN-ACK packet by the attacker box. This not only generates less traffic, but it is quicker to perform too.

In this post, we covered the basics of <span class="base0A">Nmap</span>, from simple ping scans to detailed TCP connect and SYN scans. 🚀

Always remember to use <span class="base0A">Nmap</span> ethically and legally. 🔒

Stay tuned for more on advanced features and security tips! 🔍✨

Happy scanning! 🖥️🔍
