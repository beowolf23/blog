0xBeowolf23

Hi, I am Mihai, a Computer Science :rocket: student passionate about writing scripts and now finding my way through Penetration Testing :lock: and Cyber Security. I am currently exploring various topics within Cyber Security :male_detective:, such as network security :computer:, ethical hacking, and vulnerability assessment. 

Welcome to my blog :smile:.
