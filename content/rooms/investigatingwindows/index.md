+++
title = 'Investigating Windows'
date = 2024-06-24T10:15:24+03:00
description = 'Tackling the Investigating Windows room from TryHackMe to gain knowledge about Windows forensics'
draft = false
+++

![Investigating Windows](./images/room.png)

In today's blog post, we'll explore the "Investigating Windows" room on TryHackMe. This room is designed to teach you the fundamentals of Windows forensics, focusing on analyzing various artifacts to uncover evidence of malicious activity. Whether you're new to digital forensics or looking to sharpen your skills, this room offers a hands-on approach to learning.

### Room Overview

**URL to the room**: [Investigating Windows](https://tryhackme.com/r/room/investigatingwindows)   
**Topics Covered**: Windows Forensics, Event Logs, Registry Analysis, File System Analysis  
**Prerequisites**: Basic understanding of Windows operating systems and familiarity with forensic concepts.

### Setup Instructions

To begin, you'll need a TryHackMe account. Follow these steps to get started:
1. Create a TryHackMe account if you don't have one.
2. Connect to the TryHackMe network using the OpenVPN configuration provided on the website.
    * Run the following command in your terminal: sudo apt install openvpn
    * Locate the full path to your VPN configuration file (normally in your ~/Downloads folder).
    * Use your OpenVPN file with the following command: sudo openvpn /path/to/file.ovpn
3. Navigate to the [Investigating Windows](https://tryhackme.com/r/room/investigatingwindows) room and click "Join Room".

Ensure you have access to tools like Event Viewer, Registry Editor, and other forensic utilities, which will be essential for completing the tasks.

### Walkthrough

#### <span class="base0A"> Task 1: Whats the version and year of the windows machine? </span>

The `systeminfo` command provides detailed information about the Windows operating system, including the OS version, manufacturer, system model, BIOS version, and more.

```
C:\Users\Administrator>systeminfo

Host Name:                 EC2AMAZ-I8UHO76
OS Name:                   Microsoft Windows Server 2016 Datacenter
OS Version:                10.0.14393 N/A Build 14393
OS Manufacturer:           Microsoft Corporation
```

:rocket: :rocket: **Answer: Windows Server 2016** :rocket: :rocket:

#### <span class="base0A"> Task 2: Which user logged in last? </span>

**Objective**: Examine the Event Logs to identify suspicious activities.

Open Event Viewer and navigate to Windows Logs -> Security, then look for Event ID 4624 (successful login) and note the timestamps. From the Filter tab, filter by the 4624 Event ID, and in the Details section, check for TargetUserName for every entry in the logs.

![Event Viewer](./images/event_viewer.png)

**Explanation**: Event ID 4624 indicates a successful login. By analyzing the timestamps and user accounts, we can identify who logged in and when.

 :rocket: :rocket: **Answer: Administrator** :rocket: :rocket:; it's just me who logged in last :smiley:

#### <span class="base0A"> Task 3: When did John log onto the system last? </span>

If we try to filter the logs by username as in the image below, nothing will show up. No logs are found for John.
![Filter by username](./images/john_filter.png)

Another aproach is to open up a `cmd` and try the `quser` command which will show information about the current logged in user - Administrator.
Running `quser John` will return nothing, it even complains that John does not exist. Strange. I thought John is here on the server because 
`net user` returned the following:

```
C:\Users\Administrator>net user

User accounts for \\EC2AMAZ-I8UHO76

-------------------------------------------------------------------------------
Administrator            DefaultAccount           Guest
Jenny                    John
```

Hopefully, `net user` accepts the username as a command-line argument and `net user John` returns the following:

```
C:\Users\Administrator>net user John
User name                    John
Full Name                    John
Comment
User's comment
Country/region code          000 (System Default)
Account active               Yes
Account expires              Never

Password last set            3/2/2019 5:48:19 PM
Password expires             Never
Password changeable          3/2/2019 5:48:19 PM
Password required            Yes
User may change password     Yes

Workstations allowed         All
Logon script
User profile
Home directory
Last logon                   3/2/2019 5:48:32 PM

Logon hours allowed          All

Local Group Memberships      *Users
Global Group memberships     *None
```

So, it's pretty clear when John logged in last. I wonder how he bypassed Event Logs?! :cop:

 :rocket: :rocket: **Answer: 03/02/2019 5:48:32 PM** :rocket: :rocket:

#### <span class="base0A"> Task 4: What IP does the system connect to when it first starts? </span>

This task is about finding out what runs when a user logs on. There are couple of options here. First, I started with a search on 
Task Scheduler and I got to find that we have multiple jobs running on the box.

![Task Scheduler](./images/task_scheduler.png)

Inspecting the 'Clean file system' job, we find out that it runs a netcat listener on port 1348. This does not really help us now, but it will, later. :smile:

Another way to hide a startup job is by inserting commands into the Run location in Regedit. The full path is `HKEY_LOCAL_MACHINE > SOFTWARE > Microsoft > Windows > CurrentVersion > Run`. 

![Registry](./images/regedit_run.png)

We can see that there is some command executing on startup here. It's likely that the binary is pulling the users from a remote machine by running `net user`. Anyway, we got the answer.

 :rocket: :rocket: **Answer: 10.34.2.3** :rocket: :rocket:

#### <span class="base0A"> Task 5: What two accounts had administrative privileges (other than the Administrator user)? </span>

After running `net user Administrator`, I saw that he is in the Administrators group which made me think there is a command which shows what users are in a specific group.
Using `net localgroup Administrators` I managed to get all the users in the Administrators group.

```
C:\Users\Administrator>net localgroup Administrators
Alias name     Administrators
Comment        Administrators have complete and unrestricted access to the computer/domain

Members

-------------------------------------------------------------------------------
Administrator
Guest
Jenny
```

 :rocket: :rocket: **Answer: Jenny, Guest** :rocket: :rocket:

#### <span class="base0A"> Task 6: What file was the task trying to run daily? </span>

This is easy as we have already checked Task Scheduler and we know that everyday, the `Clean file system` runs the `netcat` script.

:rocket: :rocket: **Answer: nc.ps1** :rocket: :rocket:

#### <span class="base0A"> Task 7: What port did this file listen locally for? </span>

 :rocket: :rocket: **Answer: 1348** :rocket: :rocket:

#### <span class="base0A"> Task 8: When did Jenny last logon? </span>

Again, we can run `net user Jenny` to find out logon information.

```
C:\Users\Administrator>net user Jenny
User name                    Jenny
Full Name                    Jenny
Comment
User's comment
Country/region code          000 (System Default)
Account active               Yes
Account expires              Never

Password last set            3/2/2019 4:52:25 PM
Password expires             Never
Password changeable          3/2/2019 4:52:25 PM
Password required            Yes
User may change password     Yes

Workstations allowed         All
Logon script
User profile
Home directory
Last logon                   Never

Logon hours allowed          All

Local Group Memberships      *Administrators       *Users
Global Group memberships     *None
```


:rocket: :rocket: **Answer: Never** :rocket: :rocket:; Pretty easy, huh? :smiley:

#### Task 9: At what date did the compromise take place?

We can take a look at the `C:\TMP` directory where the binaries are located and check the creation date.

:rocket: :rocket: **Answer: 03/02/2019** :rocket: :rocket:

#### Task 10: During the compromise, at what time did Windows first assign special privileges to a new logon?

We know the start date of the compromise and we also know that we have to look for `assigning special privileges to a new logon`. Googling this, we find out that 
Event Logs save a special type of log for this, with `ID 4672`. This question is strange, I would have supposed that the first occurence of this kind is actually the privilege escallation timestamp, but TryHackMe decided that they want a different answer that can be found in the Hints section (00/00/0000 0:00:49 PM - they want something like this). 

![Event Logs Filtered By ID 4672](./images/event_viewer_privileges.png)

:rocket: :rocket: **Answer: 03/02/2019 4:04:49 PM** :rocket: :rocket:

#### Task 11: What tool was used to get Windows passwords?

Doing a quick search in the `C:\TMP` directory we find out the `mim` and `mim-out` files which look interesting. Opening `mim-out`, we can see that it is a Mimikatz report in which there is a password dump. 

:rocket: :rocket: **Answer: mimikatz** :rocket: :rocket:

#### Task 12: What was the attackers external control and command servers IP?

For this one, we can check the local DNS table to see if any hosts have been poisoned. The file is located at `C:\Windows\System32\drivers\etc\hosts`. 

Opening it up, we can definitely see the damage.

```
10.2.2.2	update.microsoft.com 
127.0.0.1  www.virustotal.com 
127.0.0.1  www.www.com 
127.0.0.1  dci.sophosupd.com 
10.2.2.2	update.microsoft.com 
127.0.0.1  www.virustotal.com 
127.0.0.1  www.www.com 
127.0.0.1  dci.sophosupd.com 
10.2.2.2	update.microsoft.com 
127.0.0.1  www.virustotal.com 
127.0.0.1  www.www.com 
127.0.0.1  dci.sophosupd.com 
76.32.97.132 google.com
76.32.97.132 www.google.com
```

Searching for the ip records for www.google.com we find out that they differ from the one in the hosts file.

```
(base) ➜  ~ nslookup google.com
Server:		2a02:2f0c:8000:3::1
Address:	2a02:2f0c:8000:3::1#53

Non-authoritative answer:
Name:	google.com
Address: 142.251.208.174
```

Furthermore, we can search for `76.32.97.132` on `who.is`.

```
IP Whois
NetRange:       76.32.0.0 - 76.63.255.255
CIDR:           76.32.0.0/11
NetName:        RRPRI
NetHandle:      NET-76-32-0-0-1
Parent:         NET76 (NET-76-0-0-0-0)
NetType:        Direct Allocation
OriginAS:
Organization:   Charter Communications Inc (CC-3517)
RegDate:        2006-07-05
Updated:        2006-07-05
Ref:            https://rdap.arin.net/registry/ip/76.32.0.0



OrgName:        Charter Communications Inc
OrgId:          CC-3517
Address:        6175 S. Willow Dr
City:           Greenwood Village
StateProv:      CO
PostalCode:     80111
Country:        US
RegDate:        2018-10-10
Updated:        2022-09-14
Comment:        Legacy Time Warner Cable IP Assets
Ref:            https://rdap.arin.net/registry/entity/CC-3517


OrgAbuseHandle: ABUSE19-ARIN
OrgAbuseName:   Abuse
OrgAbusePhone:  +1-877-777-2263
OrgAbuseEmail:  abuse@charter.net
OrgAbuseRef:    https://rdap.arin.net/registry/entity/ABUSE19-ARIN

OrgTechHandle: IPADD1-ARIN
OrgTechName:   IPAddressing
OrgTechPhone:  +1-866-248-7662
OrgTechEmail:  PublicIPAddressing@charter.com
OrgTechRef:    https://rdap.arin.net/registry/entity/IPADD1-ARIN
```
This is definitely not Google :cop:. This is the attacker's command and control server.

:rocket: :rocket: **Answer: 76.32.97.132** :rocket: :rocket:

#### Task 13: What was the extension name of the shell uploaded via the servers website?

My first thought that came in mind was to Google Search the following: `windows server web server`. Neither did I think IIS exists or what that is, but after reading some documentation, I found out that it is the default web server for Windows. Was this useful? Yes, it was. Searching for the default directory of IIS, which is `C:\inetpub\wwwroot` got me finding some valuable files.

```
C:\inetpub\wwwroot>dir
 Volume in drive C has no label.
 Volume Serial Number is F078-2619

 Directory of C:\inetpub\wwwroot

03/02/2019  04:47 PM    <DIR>          .
03/02/2019  04:47 PM    <DIR>          ..
03/02/2019  04:37 PM            74,853 b.jsp
03/02/2019  04:37 PM            12,572 shell.gif
03/02/2019  04:37 PM               657 tests.jsp
```

Now we are talking! Opening them up, one by one, I found out that `b.jsp` opens a remote connection that allows a client to browse and manipulate the file system of the host.

```
This JSP program allows remote web-based file access and manipulation.  
You can copy, create, move and delete files.
```
:rocket: :rocket: **Answer: .jsp** :rocket: :rocket:

#### Task 14: What was the last port the attacker opened?

With `Windows Firewall`, we can check the open ports for the inbound rules. 

![Firewall Inbound Rules](./images/firewall.png)

We can see that inbound connections through port 1337 are allowed. 

:rocket: :rocket: **Answer: 1337** :rocket: :rocket:




#### Task 15: Check for DNS poisoning, what site was targeted?

As seen in `Task 12`, the Google's website was directing us to a different website. 

:rocket: :rocket: **Answer: google.com** :rocket: :rocket: