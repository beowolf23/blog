+++
title = "About me"
description = "A brief description of myself"
date = "2024-06-09"
author = "beowolf23"
+++

I am a student at the Polytechnics University of Bucharest, where I delve into Web Applications, Cloud Computing, and Machine Learning. My curiosity drives me to constantly learn and improve my skills, whether it's through academic courses or self-directed projects. In my spare time, I enjoy participating in Capture The Flag (CTF) competitions or just solving them on webapps like Overthewire, which provide hands-on experience and a lot of fun.

### Contact and Profiles

- https://github.com/beowolf23
- https://www.linkedin.com/in/mihai-ciocan

This is where I will post stuff I find interesting about computers, software, tools, and things I enjoy learning. Stay tuned for updates on my latest projects and insights into Cyber Security.
